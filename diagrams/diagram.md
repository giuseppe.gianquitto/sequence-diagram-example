```plantuml
@startuml
title "An hypotethical example from internet (https://youtu.be/JqJpbNxIK90)"
actor "User" as User
participant "Customer Portal" as CustomerPortal 
participant "API Gateway" as APIGateway
participant "Identity Server" as IdentityServer 
participant "Backend API" as OrdersBackendAPI
database OrdersDatabase
entity "External \nShipping Service" as ExternalShippingService

autonumber
User -> CustomerPortal ++ : Orders Request
group Step: Authentication
    CustomerPortal -> APIGateway ++: GET /api/v1/orders\nBearer Token
    APIGateway -> IdentityServer ++: Authenticate Token
    IdentityServer --> IdentityServer : Validate\nToken
    IdentityServer -[#green]> APIGateway : Success, Token Validated
else Authentication Failed
    autonumber 5
    IdentityServer -[#red]> APIGateway -- : Request Failed
    APIGateway -[#red]> CustomerPortal : Authentication Failure
    CustomerPortal -[#red]> User : Login Again.
    note right : Authentication has failed
end

autonumber 6
group Get Order API
    APIGateway -> OrdersBackendAPI ++ : GET api/v1/orders
    OrdersBackendAPI --[#blue]> OrdersBackendAPI : Do Some\nProcessing
    group Database Interaction
        autonumber 8.1
        OrdersBackendAPI -> OrdersDatabase ++: Query Orders
        OrdersDatabase -> OrdersBackendAPI --: Orders
    end

    'loop
    autonumber inc A
    loop Find shipping status
        'create ExternalShippingService
        OrdersBackendAPI -> ExternalShippingService ++: Find Shipping Staus
        ExternalShippingService -> OrdersBackendAPI -- : Return Shipping Status
    end
    autonumber 9
    OrdersBackendAPI -> CustomerPortal --: Orders
end
CustomerPortal -> User --: Response
@enduml
```

